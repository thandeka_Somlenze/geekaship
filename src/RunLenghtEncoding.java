/**
 * Created by intern on 10/1/15.
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RunLenghtEncoding {
    public static String encode(String source) {
       StringBuffer dest = new StringBuffer();
        for (int i = 0; i <source.length(); i++) {
           int runLenght = 1;
            while (i+1<source.length() && source.charAt(i) == source.charAt(i+1)) {
                runLenght++;
                i++;
            }

            dest.append(runLenght);
            dest.append(source.charAt(i));
        }
        return dest.toString();


    }

    public static String decode(String source) {

        StringBuffer dest = new StringBuffer();
        Pattern pattern = Pattern.compile("[1-26]+|[a-zA-z]");
        Matcher matcher = pattern.matcher(source);
        while (matcher.find()) {
            int number =Integer.parseInt(matcher.group());
            matcher.find();
            while (number-- !=0) {
                dest.append(matcher.group());

            }
        }
        return dest.toString();
    }
    public static void main(String[] args) {
        String example ="ABBB~A~C~ED~ZE~DE";
        System.out.println(encode(example));
        System.out.println(decode(""));
    }
}
