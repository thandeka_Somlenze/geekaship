/**
 * Created by intern on 9/21/15.
 */
public class Sumofdigits {

    public  static void  main(String[] args) {

        String in = new String("n");
        int n;

        System.out.print("Enter an integer: ");
        n = 10532;

         if (n <= 0)
             System.out.println("wrong integer.");
        else  {
             int sum = 0;
              while (n !=0) {

                  sum += n % 10;
                  n /=10;
              }
             System.out.println("Sum of digits: " + sum);
         }
    }
}
