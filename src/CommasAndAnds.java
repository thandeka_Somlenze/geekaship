/**
 * Created by intern on 9/30/15.
 */
import java.util.Scanner;
public class CommasAndAnds {
    public static String quibble(String[] words) {
        String qText = "{";
        for(int wIndex = 0; wIndex < words.length; wIndex++) {
            qText += words[wIndex] + (wIndex == words.length-1 ? "" :
                    wIndex == words.length-2 ? " and " :
                            ", ");

        }
        qText += "}";
        return qText;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println(" use lower cases \n");

        System.out.println("Please enter your name ");
        String name = input.nextLine();

        System.out.println("Please enter your surname");
        String surname = input.nextLine();

        System.out.println("Enter your age");
        String age  = input.nextLine();

        System.out.println(quibble(new String[]{}));
        System.out.println(quibble(new String[]{name}));
        System.out.println(quibble(new String[]{name, surname}));
        System.out.println(quibble(new String[]{name, age, surname}));

    }
}

