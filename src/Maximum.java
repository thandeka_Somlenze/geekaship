/**
 * Created by intern on 9/22/15.
 */
public class Maximum {
    public static void main(String[] args) {

        int[] numbers = {6, 20, 7, 80, 60};

        int max = 0;

        for (int i = 0; i < numbers.length; i++) {

            if (numbers[i] > max) {

                max = numbers[i];
            }

        }
        System.out.println("maximum number in array : " + max);

    }
}
