/**
 * Created by intern on 10/9/15.
 */
import java.util.*;
public class BruteForce {

    public static void main(String args[]){
        BruteForce src = new BruteForce();
        Scanner sc = new Scanner(System.in);
        String text = "With God everything is possible";
        System.out.print("Enter the word you want to search: ");
        String pattern = sc.next();

        src.setString(text, pattern);
        int position = src.search();

        if(position != -1)
            System.out.println("The text: "+ pattern + " is found at position " + position);
        else
            System.out.print("The text: " + pattern + " - was not found");
    }
    char[] text, pattern;
    int intText, intPattern;

    public void setString(String textString,String patternString){
        text = textString.toCharArray();
        pattern = patternString.toCharArray();
        intText = textString.length();
        intPattern = patternString.length();
    }
    public int search() {
        for (int i = 0; i < intText - intPattern; i++) {
            int j = 0;
            while (j < intPattern && text[i+j] == pattern[j]) {
                j++;
            }
            if (j == intPattern) return i;
        }
        return -1;
    }
}


